/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mesaya.oxprogramoop;

import java.lang.ref.Cleaner;

/**
 *
 * @author 10.1803
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastcol;
    private int lastrow;
    private Player winner;
    private boolean Finish = false;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col; // checkwin 
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setstatwinlose();
    }

    private void setstatwinlose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setstatwinlose();

    }

    void checkX() {
        if (table[1][1] == table[0][0]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            Finish = true;
            winner = currentPlayer;

        }
        if (table[0][2] == table[1][1]
                && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            Finish = true;
            winner = currentPlayer;

        }
        return;

    }

    void checkDraw() {
        playerO.draw();
        playerX.draw();
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;
    }
}
